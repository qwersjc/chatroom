# README #

This readme is for the chatroom application made up by server.c, client.c, and
their respective header files.

### Usage ###

* To use the chatroom, run the makefile with `make`.
* Port numbers can be specified (>2000), but the default port is 9001.
* Start the server, then run the client.

./server
./client (on a different terminal)

* The telnet application also works in connecting with the server
	- The client only works through localhost, however telnet may work over
	  the network.

* There is also a DEBUG flag for the insertion of debugging statements.

### What is this repository for? ###

* Quick summary - 
This is a chatroom application framework that supports multiple chatrooms and
clients through a central server. The max number of chatrooms and clients can
be configured by editing the header file server.h.
Mostly though, it is a final project for NCP.

* Version: 0.01a



### Who do I talk to? ###

* Spencer Chang, Matthew Chan, Sriram Gidugu


/* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) */
