#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "client.h"

#define DEBUG 0
#define KNOWN_PORT 9001
#define BUFSIZE 1024

/* Begin client variables */
static int SERVER_UP = 0;
static int SFD = 0;
static sockaddr_in SERVADDR;
static int IS_CHATTING = 0;
static int HAS_QUIT = 0;
static pthread_mutex_t chat_mutex;
static pthread_mutex_t quit_mutex;
/* End client variables */


/* throws an error, helpful for debugging */
void throwError(int line, char* file, int err){
	fprintf(stderr, "Error in %s, line %d: %s\n", file, line, strerror(err));
	exit(1);
}

/* checks and changes whether the client has quit */
int quit_status(int qs){
	if((qs==0) || (qs==1)){ //setting
		pthread_mutex_lock(&quit_mutex);
		HAS_QUIT = qs;
		pthread_mutex_unlock(&quit_mutex);
		return HAS_QUIT;
	}
	else return HAS_QUIT;
}

/* checks and changes whether the client is chatting */
int chat_status(int cs){
	if((cs==0) || (cs==1)){ //setting
		pthread_mutex_lock(&chat_mutex);
		IS_CHATTING = cs;
		pthread_mutex_unlock(&chat_mutex);
		return IS_CHATTING;
	}
	else return IS_CHATTING;
}

// TODO: fix this, possibly with some server response
/* checks to see if some status has changed */
void has_status_changed(char* msg){
	if(strncmp("!leave", msg, 6)==0) chat_status(0);
	else if(strncmp("join", msg, 4)==0) chat_status(1);
	else if(strncmp("make", msg, 4)==0) chat_status(1);
	else if(!chat_status(-1) && strncmp("quit", msg, 4)==0) quit_status(1);
}

/* creates a socket and a connection */
int create_connection(int port, sockaddr_in serveraddr_in){
	int clientfd;
	sockaddr* serveraddr = (sockaddr*)&serveraddr_in;

	if((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){ throwError(__LINE__, __FILE__, errno);
	}

	memset(serveraddr, 0, sizeof(serveraddr_in));
	serveraddr_in.sin_family = AF_INET;
	serveraddr_in.sin_port = htons(port);

	/* Establish a connection with the server */
	if (connect(clientfd, serveraddr, sizeof(serveraddr_in)) < 0){
		if(errno != ECONNREFUSED) throwError(__LINE__, __FILE__, errno);
		else{
			fprintf(stderr, ">> Could not connect to server on port %d.\n", port);
			fprintf(stderr, ">> Please make sure the port number is correct and the server is running.\n");
			exit(1);
		}
		return -1;
	}else{
		if(DEBUG) printf(">> connected; clientfd = %d\n", clientfd);
		SERVER_UP = 1;
	}
	return clientfd;
}

/* thread function that checks for and prints server messages */
void run_message_getter(void* args){

	int recvfd = *(int*) args;
	int bR; char buf[BUFSIZE];
	int chat = 0;

	if(DEBUG) printf(">> Thread: Made a new thread!\n");
	while(1){
		memset(buf, 0, BUFSIZE);
		if((bR=read(recvfd, buf, BUFSIZE)) < 0){
			throwError(__LINE__, __FILE__, errno);
		}
		if(bR==0 || strncmp("quit", buf, 4)==0){ //occurs when client exits
			quit_status(1);
			break;
		}
		printf("%s", buf);
}
	if(DEBUG) printf(">> Thread: exiting.\n");
	pthread_exit(0);
}

/* made to prevent server crashing - semi-working */
/* signal function handling SIGINT */
void cleanup(){
	quit_status(1);
	if(IS_CHATTING){
		write(SFD, "!leave", 7);

	}
	write(SFD, "quit", 5);
	sleep(1);
	//struct timespec slp;
	//slp.tv_nsec = 50000;
	//nanosleep(slp, NULL);
	//close(SFD);

	_exit(0);
}

int main(int argc, char** argv){

    /* Checking Arguments */
	unsigned short port = -1;
    if (argc < 3){
		if(argc == 2) port = atoi(argv[1]);
		else port = KNOWN_PORT;
    }else{
		fprintf(stderr, "Usage: %s [port]\n", argv[0]);
		exit(1);
	}
	if(port < 2000 || port > 65535){ // lower ports reserved
		fprintf(stderr, "Invalid port (%d)\n", port);
		fprintf(stderr, "Please use a port between 2000,65535\n");
		exit(1);
	}
	if(DEBUG) printf(">> port: %d\n", port);

	/* Initializing variables */
	int sockfd = -1;
	sockfd = create_connection(port, SERVADDR);
	char msg[BUFSIZE];
	int bR, bW;

	SFD = sockfd; //for signal handler
	signal(SIGINT, cleanup); //prevents server from crashing - mostly works

	/* Making a Reading Thread & Mutexes */
	pthread_t tid;
	pthread_create(&tid, NULL, (void*)&run_message_getter, (void*)(&sockfd));
	pthread_detach(tid);
	pthread_mutex_init(&chat_mutex, NULL);
	pthread_mutex_init(&quit_mutex, NULL);

	/* signal for ctrl-C cleanup */

	while(1){
		sleep(1);
		if(quit_status(-1)) break;

		/* Lets user know that they are not in chat */
		memset(msg, 0, sizeof(msg));
		if(!chat_status(-1)){
			printf("command: ");
			fflush(stdout); //flush stdout
		}

		if((bR=read(0, msg, BUFSIZE)) < 0) throwError(__LINE__, __FILE__, errno);
		//if(DEBUG) printf(">> read(%d):[%s]\n", bR, msg);
		if((bW=write(sockfd, msg, bR)) < 0) throwError(__LINE__, __FILE__, errno);
		has_status_changed(msg); //checks enter/leave chat && quit
		if(quit_status(-1)) break;
		
	} //end while
	close(sockfd);
	return 0;
} //end main

