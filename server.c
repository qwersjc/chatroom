#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "server.h"

#define DEBUG 0
// already defined in the server
#ifndef SERVER_H
	#define CLIENT_LIMIT 30
	#define CHATROOM_LIMIT 3
	#define KNOWN_PORT 9001
	#define NAME_LEN 32
	#define BUFSIZE 1024
#endif


//TODO: changed fixed port - done
//TODO: make output aesthetically pleasing - done
//TODO: implement change name - done
//TODO: clean empty chats - done
//TODO: implement make(+join) chatrooms - done
//TODO: implement client - done
//TODO: implement logs - done
//TODO: remove dependency on csapp - done
//TODO: implement locks - done
//TODO: test over networks -> get hostname by address


/* Begin Server Variables */
static unsigned int CONNECTIONS = 0;
static unsigned int CHATROOMNUM = 0;
static client*   cl_list[  CLIENT_LIMIT];
static chatroom* cr_list[CHATROOM_LIMIT];
static unsigned int CHATID = 1;
static unsigned int USERID = 1;
static pthread_mutex_t cr_lock;
/* End Server Variables */

/* prints out a server update */
void server_update(){
	while(1){
		printf("\nServer Update:\n");
		printf("==========================\n");
		printf("%d clients, %d chatrooms.\n", CONNECTIONS, CHATROOMNUM);
		pthread_mutex_lock(&cr_lock);
		for(int i=0; i<CHATROOM_LIMIT; i++){
			chatroom* ch;
			if((ch=cr_list[i]) != NULL){
				printf("Chatroom %s: %d chatting.\n", ch->name, ch->users);
			}
		}
		pthread_mutex_unlock(&cr_lock);
		printf("\n");
		sleep(5);
	}

}

/* mallocs a string for a message with a time stamp */
// may be changed
char* get_time(){
	time_t now;
	char* time_str = malloc(sizeof(char)*BUFSIZE);
	memset(time_str, 0, BUFSIZE);

	now = time(NULL);
	strftime(time_str, BUFSIZE, "(%a, %b %d)%H:%M:%S", localtime(&now));

	return time_str;
}

/* makes a new chatroom struct and also puts it in the array */
chatroom* make_chatroom(char* name){
	if(CHATROOMNUM == CHATROOM_LIMIT) return NULL;
	for(int i=0; i<CHATROOM_LIMIT; i++){
		if(cr_list[i] == NULL){
			chatroom* c;
			if((c=malloc(sizeof(chatroom))) == NULL) throwError(__LINE__, __FILE__, errno);

			c->id = CHATID++;
			if(name == NULL){
				char buf[NAME_LEN];
				snprintf(buf, sizeof(buf), "Chatroom(%d)", c->id);
				strncpy(c->name, buf, sizeof(c->name));
			}else strncpy(c->name, name, sizeof(c->name));
			cr_list[i] = c;
			c->users = 0;
			c->index = i;
			CHATROOMNUM++;

			return c;
		}
	}

	return NULL;
}

/* removes chatroom, called when empty */
int remove_chatroom(chatroom* chatty){
	if(chatty == NULL) return -1;

	CHATROOMNUM--;
	cr_list[chatty->index] = NULL;
	free(chatty);

	return 0;
}

/* function for creating a new client struct */
client* new_client(int fd, sockaddr_in saddr, char* uname, int index){
	client* c;
	if((c=malloc(sizeof(client))) == NULL) throwError(__LINE__, __FILE__, errno);

	c->id = USERID++;
	if(uname == NULL){
		char buf[NAME_LEN];
		snprintf(buf, sizeof(buf), "user%d", c->id);
		strncpy(c->username, buf, sizeof(c->username));
	}else strncpy(c->username, uname, sizeof(c->username));
	c->fd = fd;
	c->sockaddr = saddr;
	c->active = -1;
	c->cr_index = -1;
	c->index = index;

	return c;
}
/* adds a client struct to the array, representing a new connection */
client* add_connection(int connfd, sockaddr_in connaddr_in, char* name){
	if(CONNECTIONS >= CLIENT_LIMIT) return NULL;
	for(int i=0; i<CLIENT_LIMIT; i++){
		if(cl_list[i] == NULL){
			client* newClient = new_client(connfd, connaddr_in, name, i);
			cl_list[i] = newClient;
			CONNECTIONS++;
			return newClient;
		}
	}
	return NULL;
}

/* removes client */
int remove_client(client* c){
	if(c == NULL) return -1;

	if(DEBUG) printf("%s (%d) disconnecting...\n", c->username, c->id);
	char* msg_bye = "\nThank you. Good bye...\n";
	write(c->fd, msg_bye, strlen(msg_bye));

	CONNECTIONS--;
	cl_list[c->index] = NULL;
	close(c->fd);
	free(c);

	return 0;
}


/* gets rid of improper characters in inputs */
char* clean(char * msg){
	if(msg == NULL) return NULL;
	char* cleaned = msg;
	while(*msg != '\0'){
		if(*msg=='\r' || *msg=='\n'){
			*msg = '\0'; //strip trailing newlines
		}else if(*msg=='\t'){
			*msg = ' '; //convert tabs to spaces
		}else if(*msg < 32 || *msg > 126){
			*msg = '?'; //removing un-displayable characters
		}
		msg++;
	}
	return cleaned;

}

/* prints contextual help to client */
void print_help(int inChat, client* cl){
	char buf[BUFSIZE];
	memset(buf, 0, BUFSIZE);

	sprintf(buf, "\n=====List of commands:\n");
	if(!inChat){
		strcat(buf, "1. help - displays help menu\n");
		strcat(buf, "2. list - list chatrooms\n");
		strcat(buf, "3. make [chatroom] - make a new chatroom\n");
		strcat(buf, "4. join [chatroom] - join a chatroom\n");
		strcat(buf, "5. name [new name] - change your name\n");
		strcat(buf, "6. quit - exit the client\n");
	}else{
		strcat(buf, "1. !help  - display chat help\n");
		strcat(buf, "2. !marco - test server response\n");
		strcat(buf, "3. !active- show chatroom users\n");
		strcat(buf, "4. !pm [name] [msg] - send a private message\n");
		strcat(buf, "5. !leave - leave chatroom\n");
	}
	strcat(buf, "======================\n\n");

	send_self(buf, cl->fd);
}

/* writes (public) chatroom messages to a log */
void write_to_log(char* msg, client* cl){
	FILE* fp;
	char fname[BUFSIZE];
	memset(fname, 0, BUFSIZE);
	if(cl!=NULL && cl->cr_index!=-1 && cr_list[cl->cr_index]!=NULL){
		strcat(fname, cr_list[cl->cr_index]->name);
		strcat(fname, "_log.txt");
		fp = fopen(fname, "a");
		fwrite(msg, 1, strlen(msg), fp);
		fclose(fp);
	}
}


/* thread function does most of the client handling */
void thread_function(void* args){

	// Prevents server from crashing when client leaves.
	// Not perfect, cannot use signal handler with SIG_IGN
	signal(SIGPIPE, SIG_IGN);

	client* cl = (client*)args;
	int connfd = cl -> fd;

	char* msg_welcome;
	if(DEBUG) msg_welcome = "\n\nWelcome to Moo-Chat Services Inc.\nChoose or create a chatroom.\n";
	else      msg_welcome = "\n\nWelcome to N-Chat-P Services Inc.\nChoose or create a chatroom.\n";
							     
	write(connfd, msg_welcome, strlen(msg_welcome));
	msg_welcome = "=================================\n";
	write(connfd, msg_welcome, strlen(msg_welcome));

	while(1){
		char* msg_instr = ">> For a list of commands type in \"help\"\n";
		write(connfd, msg_instr, strlen(msg_instr));

		char buf[BUFSIZE];
		memset(buf, 0, sizeof(buf));
		int bR = read(connfd, buf, BUFSIZE);
		if(bR == 0){ //disconnect
			printf("!! Client %s Disconnected...\n", cl->username);
			remove_client(cl);
			break;

		}
		clean(buf); //sanitizing input
		if(DEBUG) printf(">> Received: [%s]\n", buf);

		if(strncmp("list", buf, 4) == 0){
			print_crlist(connfd);
		}else if(strncmp("make", buf, 4) == 0){
			/* Making a default chatroom */
			char * chatroomname = buf + 5;
			clean(chatroomname);
			int i;
			chatroom * c;
			if(chatroomname[0]=='\0' || chatroomname[0]=='\n' || chatroomname[0]==' ' || chatroomname[0]=='\r'){
				char* msg_err = ">> Please enter a valid chatroom name.\n";
				write(connfd, msg_err, strlen(msg_err));
			}else{
				if(CHATROOMNUM == 0){
					c = make_chatroom(chatroomname);
					if(!c){
						char* msg_maxed = ">> Chatroom limit has been exceeded. Please join an existing chatroom.\n";
						write(connfd, msg_maxed, strlen(msg_maxed));
					}else{
						cl->active = c->id;
						cl->cr_index = c->index;
						chat_func(cl, c);
					}
				}else{
					int found = 0;
					for(i = 0; i < CHATROOMNUM; i++){
						if(strncmp(chatroomname, cr_list[i]->name, NAME_LEN) == 0){
							char* msg_dup = ">> This chatroom name already exists! Either join it or try a new name.\n";
							write(connfd, msg_dup, strlen(msg_dup));
							found = 1;
							break;
						}
					}
					if(found == 0){
						c = make_chatroom(chatroomname);
						if(!c){
							char* msg_maxed = ">> Chatroom limit has been exceeded. Please join an existing chatroom.\n";
							write(connfd, msg_maxed, strlen(msg_maxed));
						}else{
							cl->active = c->id;
							cl->cr_index = c->index;
							chat_func(cl, c);
						} 
					}
				}
			}
		// same as with make, move to function
		}else if(strncmp("join", buf, 4) == 0){
			int found = -1;
			char * chatname = buf + 5;
			for(int i = 0; i < CHATROOM_LIMIT; i++){
				if(cr_list[i]!=NULL && strncmp(cr_list[i]->name, chatname, NAME_LEN)==0){
					//found the chatroom!
					printf(">> Client joining a chatroom...\n");
					found = i;
					break;
				}
			}
			if(found >= 0){
				char* msg_ok = ">> OK\n";
				write(connfd, msg_ok, strlen(msg_ok));
				cl->active = cr_list[found]->id;
				cl->cr_index = cr_list[found]->index;
				chat_func(cl, cr_list[found]);
			}else{ //no chatroom by that name exists, return error message
				char* msg_nonex = ">> This chatroom does not exist. Please try again.\n";
				write(connfd, msg_nonex, strlen(msg_nonex));
			}
		}else if(strncmp("quit", buf, 4) == 0){
			remove_client(cl);
			break;
		}else if(strncmp("help", buf, 4) == 0){
			print_help(0, cl);
		}else if(strncmp("name", buf, 4) == 0){
			char * newname;
			char writeout[BUFSIZE], oldname[NAME_LEN];
			memset(writeout, 0,  strlen(writeout));
			memset(oldname, 0, strlen(oldname));
			newname = strtok(buf, " ");
			newname = strtok(NULL, " ");
			clean(newname);
			if(newname==NULL || newname[0]==' ' || newname[0]=='\n' || newname[0]=='\r' || newname[0]=='\0'){
				sprintf(writeout, ">> Your current username is %s. Please enter a valid username change command if you wish.\n", &cl->username[0]);
				write(connfd, writeout, strlen(writeout));
			}else{
				strncpy(&oldname[0], &cl->username[0], NAME_LEN);
				memset(cl->username, 0, strlen(cl->username));
				strncpy(&cl->username[0], newname, NAME_LEN);
				sprintf(writeout, ">> Your username has been changed from %s to %s.\n", oldname, newname);
				write(connfd, writeout, strlen(writeout));
			}
		}else{
			char* msg_unknown = ">> Unknown command! Please try again.\n";
			write(connfd, msg_unknown, strlen(msg_unknown));
		}
	}

	pthread_exit(0);
}

/* Prints chatroom list to file descriptor connfd */
void print_crlist(int connfd){
	char buf[BUFSIZE];
	char* cr;
	memset(buf, 0, sizeof(buf));
	int nully = 0;
	for(int i=0; i<CHATROOM_LIMIT; i++){
		if(cr_list[i] != NULL){
			nully = 1;
			cr = strncat(buf, cr_list[i]->name, BUFSIZE-1);
			cr = strncat(buf, "\n", BUFSIZE-1);
		}
	}
	if(DEBUG) printf("chatroom list:\n%s\n", buf);
	if(nully == 1){
		int bW = write(connfd, cr, BUFSIZE);
		nully = 0;
	}else{
		char* msg_nochat = ">> There are currently no chatrooms in session.\n>> Please make one to begin chatting!\n";
		int bW = write(connfd, msg_nochat, strlen(msg_nochat));
	}
}

/* throws an error, helpful for debugging */
void throwError(int line, char* file, int err){
	fprintf(stderr, "Error in %s, line %d: %s\n", file, line, strerror(err));
	exit(1);
}

/* sends message all people in a chatroom */
void send_all(char * msg, client * cl){
	int i;
	char* timey = get_time();
	strcat(timey, ": ");
	strcat(timey, msg);
	strcat(timey, "\n");
	for(i = 0; i < CLIENT_LIMIT; i++){
		if(cl_list[i]){
			//client exists
			if(cl_list[i]->active == cl->active){
				//client in same chatroom
				write(cl_list[i]->fd, timey, strlen(timey));
			}
		}
	}
	free(timey);
}

/* sends message to all users */
void send_everyone(char * msg, client * cl){
	char* timey = get_time();
	strcat(timey, ": ");
	strcat(timey, msg);
	strcat(timey, "\n");
	int i;
	for(i = 0; i < CLIENT_LIMIT; i++){
		if(cl_list[i]){
			//client exists
			write(cl_list[i]->fd, timey, strlen(timey));
		}
	}
	free(timey);
}

/* sends message to self */
void send_self(const char *string, int fd){
	write(fd, string, strlen(string));
}

/* sends a list of people in current chatroom */
void send_active(int fd, int active){
	int i;
	char string[BUFSIZE];
	for(i = 0; i < CLIENT_LIMIT; i++){
		if(cl_list[i] && cl_list[i]->active==active){
			sprintf(string, ">> CLIENT %d | %s\n", cl_list[i]->id, &cl_list[i]->username[0]);
			send_self(string, fd);
		}
	}
}

/* sends msg to everyone else in chatroom */
/* also writes to log */
void send_msg(char *string, client * cl){
	int i;
	char* timey = get_time();
	strcat(timey, ": ");
	strcat(timey, string);
	strcat(timey, "\n");
	for(i = 0; i < CLIENT_LIMIT; i++){
		if(cl_list[i]){
			if(cl_list[i]->active == cl->active){
				if(cl_list[i]->id != cl->id){
					write(cl_list[i]->fd, timey, strlen(timey));
				}
			}
		}
	}
	write_to_log(timey, cl);
	free(timey);
}

/* function run by thread function when user is in chat mode */
void chat_func(client * cl, chatroom * chatty){
	char buff_out[BUFSIZE];
	char buff_in[BUFSIZE];
	int bR;
	memset(buff_out, 0, sizeof(buff_out));
	memset(buff_in, 0, sizeof(buff_out));

	sprintf(buff_out, ">> Hi %s, welcome to chatroom %s.\n", cl->username, &chatty->name[0]);
	strcat(buff_out, ">> Type '!help' for help\n");
	printf("User %s has joined chatroom %s.\n", cl->username, &chatty->name[0]);
	write(cl->fd, buff_out, strlen(buff_out));
	memset(buff_out, 0, strlen(buff_out));
	chatty->users++;

	sprintf(buff_out, "User %s has joined this chat. Say hello!\n", &cl->username[0]);
	send_msg(buff_out, cl);

	while((bR=read(cl->fd, buff_in, sizeof(buff_in)-1)) > -1){
		if(bR == 0) goto leaving;
		buff_in[bR] = '\0';
		buff_out[0] = '\0';
		clean(buff_in);
		if(strlen(buff_in) == 0) continue; //gets rid of empty messages in chat

		if(buff_in[0] == '!'){
			char *command, *param;
			command = strtok(buff_in, " ");
			if(!strcmp(command, "!leave")){
leaving:
				chatty->users--;
				memset(buff_out, 0, strlen(buff_out));
				sprintf(buff_out, "%s has left this chatroom.\n", &cl->username[0]);
				send_msg(buff_out, cl); // if last person, sends to nobody but logs leaving
				printf("%s", buff_out);
				cl->active = -1;
				cl->cr_index = -1;
				if(chatty->users == 0){
					if(DEBUG) printf(">> Cleaning up empty chatroom.\n");
					remove_chatroom(chatty);
				}
				return;
			}else if(!strcmp(command, "!marco")){
				send_self("!polo\n", cl->fd);
			}else if(!strcmp(command, "!active")){
				sprintf(buff_out, ">> ChatPeeps %d\n", chatty->users);
				send_self(buff_out, cl->fd);
				send_active(cl->fd, cl->active);
			}else if(!strcmp(command, "!pm")){
				param = strtok(NULL, " ");
				if(param){
					char uname[strlen(param)];
					strcpy(&uname[0], param);
					//int uid = atoi(param);
					param = strtok(NULL, " ");
					if(param){
						sprintf(buff_out, "[PM][%s]", cl->username);
						while(param != NULL){
							strcat(buff_out, " ");
							strcat(buff_out, param);
							param = strtok(NULL, " ");
						}
						int i;
						for(i = 0; i < CLIENT_LIMIT; i++){
							if(cl_list[i] == NULL){
								continue;
							}else{
								if(strcmp(cl_list[i]->username, &uname[0]) == 0){
									char * tstring = get_time();
									strcat(tstring, ": ");
									strcat(buff_out, "\n");
									//printf("TString: %s\n", tstring);
									//printf("Buff_out: %s\n", &buff_out[0]);
									printf("PM Sent\n");
									write(cl_list[i]->fd, tstring, strlen(tstring));
									write(cl_list[i]->fd, buff_out, strlen(buff_out));
									memset(buff_out, 0, strlen(buff_out));
									break;
								}
							}
						}
					}else{
						send_self(">> No message to send\n", cl->fd);
					}
				}else{
					send_self(">> No recipient specified\n", cl->fd);
				}

			}else if(!strcmp(command, "!help")){
				print_help(1, cl);
			}else{
				send_self(">> Unknown Command\n", cl->fd);
			}
		}else{ //sending normal message
			sprintf(buff_out, "[%s] %s", &cl->username[0], buff_in);
			send_msg(buff_out, cl);
		}
	}
	return;


}


/* function included from csapp to avoid including entire library */
/* opens a listening socket on port, returns fd */
int open_listenfd(int port){
    int listenfd, optval=1;
    sockaddr_in serveraddr;
  
    /* Create a socket descriptor */
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	return -1;
 
    /* Eliminates "Address already in use" error from bind. */
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int)) < 0)
	return -1;

    /* Listenfd will be an endpoint for all requests to port
       on any IP address for this host */
    memset((char *) &serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET; 
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    serveraddr.sin_port = htons((unsigned short)port); 
    if (bind(listenfd, (sockaddr*)&serveraddr, sizeof(serveraddr)) < 0)
	return -1;

    /* Make it a listening socket ready to accept connection requests */
    if (listen(listenfd, 1024) < 0)
	return -1;
    return listenfd;
}


int main(int argc, char** argv){

    /* Checking Arguments */
	unsigned short port = -1;
    if (argc < 3){
		if(argc == 2) port = atoi(argv[1]);
		else port = KNOWN_PORT;
    }else{
		fprintf(stderr, "Usage: %s [port]\n", argv[0]);
		exit(1);
	}
	if(port < 2000 || port > 65535){ // lower ports are usually reserved
		fprintf(stderr, "Invalid port (%d)\n", port);
		fprintf(stderr, "Please use a port between 2000,65535\n");
		exit(1);
	}
	if(DEBUG) printf(">> port: %d\n", port);

	/* Creating Listening Socket */
	int listenfd = -1;
	if((listenfd=open_listenfd(port)) < 0){
		throwError(__LINE__,__FILE__,errno);
	}
	if(DEBUG) printf(">> listenfd: %d\n", listenfd);

	/* Initializing locks */
	pthread_mutex_init(&cr_lock, NULL);



	/* Starting Server Update Thread */
	pthread_t tup;
	pthread_create(&tup, NULL, (void*)&server_update, NULL);
	pthread_detach(tup);

	/* Accepting Connections */
	while(1){
		printf(">> Server Listening on port %d...\n", port);

		int connfd;
		sockaddr_in connaddr_in;
		sockaddr* connaddr = (sockaddr*) &connaddr_in;
		socklen_t clilen;

		memset(&connaddr_in, 0, sizeof(connaddr_in));

		if((connfd=accept(listenfd, connaddr, &clilen)) < 0){
			throwError(__LINE__,__FILE__,errno);
		}

		client* newClient = add_connection(connfd, connaddr_in, NULL);
		if(newClient == NULL){
			printf(">> Connection rejected.(%d)\n", CONNECTIONS);
			char* msg_quit = "quit: Sorry, max clients reached\n";
			write(connfd, msg_quit, strlen(msg_quit));
			//write to client about error?

		}else{
			printf(">> Connection accepted.(%d)\n", CONNECTIONS);

			/* Creating a thread to interface with the client */
			pthread_t tid;
			pthread_create(&tid, NULL, (void*)&thread_function, (void*)newClient);
			pthread_detach(tid);
		}
		if(DEBUG) printf("This is how many connections exist now: %d\n", CONNECTIONS);
	}

	return 0;
}


