#ifndef SERVER_H
#define SERVER_H

#define CLIENT_LIMIT 40
#define CHATROOM_LIMIT 10
#define KNOWN_PORT 9001
#define NAME_LEN 64
#define BUFSIZE 1024

typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;

typedef struct chatroom{
	int id;
	char name[NAME_LEN];
	int index;
	int users;
	int userlist[CLIENT_LIMIT];

} chatroom;

typedef struct client{
	int id;
	char username[NAME_LEN];
	int index;
	int cr_index; //index of chatroom
	int fd;
	sockaddr_in sockaddr;
	int active; //also chatroom id 

} client;

/* prints out a server update */
void server_update();

/* makes a new chatroom struct and also puts it in the array */
chatroom* make_chatroom(char* name);

/* removes chatroom, called when empty */
int remove_chatroom(chatroom* chatty);

/* function for creating a new client struct */
client* new_client(int fd, sockaddr_in saddr, char* uname, int index);

/* adds a client struct to the array, representing a new connection */
client* add_connection(int connfd, sockaddr_in connaddr_in, char* name);

/* removes client when they leave */
int remove_client(client* c);




/* gets rid of improper characters in inputs */
char* clean(char * msg);

/* throws an error, helpful for debugging */
void throwError(int line, char* file, int err);

/* mallocs a string for a message with a time stamp */
char* get_time(); // may be changed

/* prints contextual help to client */
void print_help(int inChat, client* cl);

/* writes (public) chatroom messages to a log */
void write_to_log(char* msg, client* cl);

/* Prints chatroom list to file descriptor connfd */
void print_crlist(int connfd);




/* thread function does most of the client handling */
void thread_function(void* args);

/* function run by thread function when user is in chat mode */
void chat_func(client * cl, chatroom * chatty);




/* sends message all people in a chatroom */
void send_all(char * msg, client * cl); //maybe replace with send_msg + send_self ?

/* sends message to all users */
void send_everyone(char * msg, client * cl);

/* sends message to self */
void send_self(const char *string, int fd);

/* sends a list of people in current chatroom */
void send_active(int fd, int active);

/* sends msg to everyone else in chatroom */
/* also writes to log */
/* this is used during normal chat operations where all clients in the chatroom get informed of the message from a client*/
void send_msg(char *string, client * cl);




/* function included from csapp to avoid including entire library */
/* opens a listening socket on port, returns fd */
int open_listenfd(int port);


#endif
