#ifndef CLIENT_H
#define CLIENT_H

typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;


/* throws an error, helpful for debugging */
void throwError(int line, char* file, int err);

/* checks and changes whether the client has quit */
int quit_status(int qs);

/* checks and changes whether the client is chatting */
int chat_status(int cs);

/* checks to see if some status has changed */
void has_status_changed(char* msg);

/* creates a socket and a connection */
int create_connection(int port, sockaddr_in serveraddr_in);

/* made to prevent server crashing - semi-working */
/* signal function handling SIGINT */
void cleanup();




/* thread function that checks for and prints server messages */
void run_message_getter(void* args);

#endif
