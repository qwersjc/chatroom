# Makefile for chatroom

CC = gcc
# CFLAGS = -std=c99 -Wall -g 
CFLAGS = -std=c99 -g 
LDFLAGS = -lpthread

OBJS1 = server.o
OBJS2 = client.o

all: server client

server: $(OBJS1)
	$(CC) $(OBJS1) $(LDFLAGS) -o server

client: $(OBJS2)
	$(CC) $(OBJS2) $(LDFLAGS) -o client

client.o: client.c client.h
	$(CC) $(CFLAGS) -c client.c

server.o: server.c server.h
	$(CC) $(CFLAGS) -c server.c

clean:
	rm -f *~ *.o client server

